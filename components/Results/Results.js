import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Chip } from 'react-mdl';
import s from './results.css';

function compare(a, b) {
  if (Array.isArray(a) && Array.isArray(b)) {
    if (a.length != b.length) return false;
    for (let i=0; i<a.length; i++) {
      if (a[i] != b[i])
        return false;
    }
    return true;
  }
  return a == b;
}

function isNotSelected(selection) {
  if (Array.isArray(selection)) {
    for (const v of selection) {
      if (v !== false) return false;
    }
    return true;
  }
  return selection === -1;
}

function Results({ quizzes }) {
  return <div>
    {
      quizzes.map((quiz) => <table key={quiz.id} className={`mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp ${s.table}`}>
        <thead>
          <tr>
            <th className={`mdl-data-table__cell--non-numeric ${s.max}`}>Question</th>
            <th className="mdl-data-table__cell--non-numeric">Answer</th>
            <th>Is correct</th>
          </tr>
        </thead>
        <tbody>
          {
            quiz.questions.map((q, i) => <tr key={i}>
              <td className={`mdl-data-table__cell--non-numeric ${s.max}`}>{q.question}</td>
              <td className={`mdl-data-table__cell--non-numeric ${s.multi}`}>{
                isNotSelected(q.selection) ?
                  '―'
                :
                  q.multipleChoice ?
                    q.selection.map((cur, j) => cur ? <Chip key={j}>{q.answers[j]}</Chip> : '')
                  :
                    q.answers[q.selection]
              }</td>
              <td className={ isNotSelected(q.selection) ? '' : (compare(q.selection, q.correct) ? s.correct : s.incorrect) }>{
                isNotSelected(q.selection) ? '―' :
                  compare(q.selection, q.correct) ? 'yes' : 'no'
              }</td>
            </tr>)
          }
        </tbody>
      </table>)
    }
  </div>;
}

const mapState = (state) => ({
  quizzes: state.quiz.toJS(),
});

export default connect(mapState)(Results);
