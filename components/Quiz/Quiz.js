import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Radio, RadioGroup, Checkbox, Card, CardTitle, CardText } from 'react-mdl';
import { selectAnswer } from '../../actions/quiz';
import s from './Quiz.css';

class Quiz extends React.Component {
  static propTypes = {
    quiz: PropTypes.string.isRequired,
    questions: PropTypes.arrayOf(PropTypes.shape({
      question: PropTypes.string.isRequired,
      answers: PropTypes.arrayOf(PropTypes.string),
      multipleChoice: PropTypes.bool.isRequired,
      selection: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.arrayOf(PropTypes.bool),
      ]).isRequired,
    })),
    selectAnswer: PropTypes.func.isRequired,
  };

  constructor(...args) {
    super(...args);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleRadio = this.handleRadio.bind(this);
  }

  handleCheck(e) {
    const [question, answer] = e.target.value.split(':');
    const selection = this.props.questions[question]['selection'].slice();
    selection[answer] = !this.props.questions[question]['selection'][answer];
    this.props.selectAnswer(this.props.quiz, question, selection);
  }

  handleRadio(e) {
    const [question, answer] = e.target.value.split(':');
    this.props.selectAnswer(this.props.quiz, question, parseInt(answer));
  }

  render() {
    const { questions } = this.props;
    return <div className={s.quiz}>
      {
        questions.map((q, k) => <Card key={k} shadow={1} className={s.quizQuestion}>
          <CardTitle className={s.quizTitle}>{`${k+1}. ${q.question}`}</CardTitle>
          <CardText className={s.quizAnswers}>
            {
              q.multipleChoice ?
                q.answers.map((a, j) =>
                  <Checkbox
                    className={s.answer}
                    value={`${k}:${j}`}
                    onChange={this.handleCheck}
                    checked={q.selection[j]}
                    label={a}
                    key={j}
                    ripple
                  />
                )
              :
              <RadioGroup name={`quiz-${k}`} value={`${k}:${q.selection}`} onChange={this.handleRadio}>
                {
                  q.answers.map((a, j) =>
                    <Radio
                      className={s.answer}
                      value={`${k}:${j}`}
                      key={j}
                      ripple
                    >{ a }</Radio>
                  )
                }
              </RadioGroup>
            }
          </CardText>
          </Card>
        )
      }
    </div>;
  }
}

const mapState = (state, ownProps) => ({
  questions: state.quiz
    .find((quiz) => quiz.get('id') === ownProps.quiz)
    .get('questions')
    .toJS()
});

const mapDispatch = {
  selectAnswer,
};

export default connect(mapState, mapDispatch)(Quiz);
