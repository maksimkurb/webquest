import { combineReducers } from 'redux';

import quest from './quest';
import quiz from './quiz';

export default combineReducers({
  quest,
  quiz,
});
