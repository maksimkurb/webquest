
import {
  QUESTS_PENDING,
  QUESTS_FULFILLED,
  QUESTS_REJECTED,
} from '../core/constants';

const initialState = {
  loading: false,
  error: null,
  steps: null,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case QUESTS_PENDING:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case QUESTS_FULFILLED:
      return {
        ...state,
        loading: false,
        error: null,
        steps: action.payload,
      };
    case QUESTS_REJECTED:
      return{
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}
