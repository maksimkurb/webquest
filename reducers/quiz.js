import Immutable from 'immutable';

import {
  QUIZ_SELECT_ANSWER,
  QUIZ_POPULATE_KEYS,
} from '../core/constants';

const initialState = null;
  /**
   *  [ 0: {
   *     id: "someQuizId",
   *     questions: [ 0: {
   *       question: "Some question?",
   *       answers: [
   *         "answer1",
   *         "answer2",
   *         "answer3"
   *       ],
   *       correct: [false, true, false],
   *       multipleChoice: true/false,
   *
   *       selection: [true, false, true] | 2
   *     } ]
   *   } ]
   */

export default function (state = initialState, action) {
  switch (action.type) {
    case QUIZ_SELECT_ANSWER:
      const { quiz, question, selection } = action.payload;
      const quizIndex = state
        .findIndex((q) => q.get('id') === quiz);
      return state
        .setIn([quizIndex, 'questions', question, 'selection'], selection);
    case QUIZ_POPULATE_KEYS:
      return Immutable.fromJS(action.payload);
    default:
      return state;
  }
}
