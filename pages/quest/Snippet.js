import React, { PropTypes } from 'react';
import ReactMarkdown from 'react-markdown';
import Quiz from '../../components/Quiz';
import Results from '../../components/Results';
import s from './styles.css';

const quizMask = /<([\w]+)(?:\s+)?(?:[\w=]+["']([\w\s]+)[\s"']+)?\/?>/i;
function HtmlRenderer({literal, isBlock}) {
  const matches = quizMask.exec(literal);
  if (matches && matches.length > 1) {
    if (matches[1] === 'quiz') {
      const quizId = matches[2];
      return <Quiz quiz={quizId} />
    } else if (matches[1] === 'results') {
      return <Results/>;
    }
  }
  return React.createElement(isBlock ? 'div' : 'span', {dangerouslySetInnerHTML:{__html:literal}});
}

function Snippet({snippet, title}) {
  return <div>
    <h2 className={s.header}>{title}</h2>
    <ReactMarkdown className={s.snippet} source={snippet} renderers={{html_inline: HtmlRenderer, html_block: HtmlRenderer}} />
  </div>;
}

Snippet.propTypes = {
  snippet: PropTypes.string,
  title: PropTypes.string,
};

export default Snippet;
