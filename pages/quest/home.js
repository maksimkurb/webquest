import React from 'react';

import history from '../../core/history';
import { pages } from '../../quest/settings.yml';

export default function() {
  history.push(`/${pages[0] || 'error'}`);
  return <div></div>;
}
