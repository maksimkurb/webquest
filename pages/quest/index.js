/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import { ProgressBar } from 'react-mdl';
import { connect } from 'react-redux';
import { fetchQuests } from '../../actions/quest';
import { loadQuizzes } from '../../actions/quiz';
import Layout from '../../components/Layout';
import ErrorPage from '../../pages/error';
import Menu from './Menu';
import Snippet from './Snippet';
import { title } from '../../quest/settings.yml';
import s from './styles.css';

class HomePage extends React.Component {

  static propTypes = {
    route: PropTypes.object.isRequired,
    fetchQuests: PropTypes.func.isRequired,
    stage: PropTypes.shape({
      title: PropTypes.string,
      md: PropTypes.string.isRequired,
    }),
    pages: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired,
      title: PropTypes.string,
    })),
  };

  constructor({ fetchQuests, loadQuizzes, ...rest }) {
    super(...rest);
    fetchQuests();
    loadQuizzes();
  }

  componentDidMount() {
    const { stage } = this.props;
    document.title = (stage && stage.title + ' - ' || '') + (title || 'Web Quest');
  }
  componentWillUpdate(nextProps) {
    const { stage } = nextProps;
    document.title = (stage && stage.title + ' - ' || '') + (title || 'Web Quest');
  }

  render() {
    const { stage, pages, loading, error } = this.props;
    if (loading) {
      return <Layout className={s.content}>
        <h4 className={s.center}>Loading</h4>
        <ProgressBar className={s.progress} indeterminate />
      </Layout>;
    }
    if (!stage || error) {
      return <ErrorPage error={ error || {status: 404}}/>;
    }
    return (
      <Layout className={s.content}>
        <div className={s.side}>
          <Menu pages={pages} />
        </div>
        <div className={s.container}>
          <Snippet title={stage.title} snippet={stage.md} />
        </div>
      </Layout>
    );
  }

}

const mapState = (state, ownProps) => ({
  stage: state.quest.steps && state.quest.steps[ownProps.route.params.stage],
  pages: state.quest.steps && Object.entries(state.quest.steps).map(([name, step]) => {
    return {
      name: name,
      title: step.title || name,
    };
  }) || [],
  loading: state.quest.loading || !state.quest.steps || !state.quiz,
  error: state.quest.error,
});

const mapDispatch = {
  fetchQuests,
  loadQuizzes,
};

export default connect(mapState, mapDispatch)(HomePage);
