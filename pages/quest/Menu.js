import React, { PropTypes } from 'react';
import { Button } from 'react-mdl';
import Link from '../../components/Link';
import s from './styles.css';

function Menu({pages}) {
  return <ul>
    {
      pages.map((page) => {
        return <li key={page.name}>
          <Button className={s.menuButton} ripple component={Link} to={`/${page.name}`}>
            {page.title || page.name}
          </Button>
        </li>;
      })
    }
  </ul>;
}

Menu.propTypes = {
  pages: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    title: PropTypes.string,
  })).isRequired,
};

export default Menu;
