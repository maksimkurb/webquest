
/**
 * Converts quizzes objects to normal array
 *
 *   "someQuizID": {
 *     "Some question?": {
 *       "answer1": false,
 *       "answer2": true,
 *       "answer3": false,
 *     }
 *   }
 *
 * becomes
 *
 *   [{
 *     id: "someQuizId",
 *     questions: [
 *       question: "Some question?",
 *       answers: [
 *         "answer1",
 *         "answer2",
 *         "answer3"
 *       ],
 *       correct: [false, true, false]
 *     ]
 *   }]
 *
 */
module.exports = function settingsLoader(source) {
  this.cacheable();

  const settings = JSON.parse(source);
  const quizzes = Object.keys(settings.quizzes).map(function (quizId) {
    const questions = settings.quizzes[quizId];
    return {
      id: quizId,
      questions: Object.keys(questions).map(function (questionKey) {
        const answers = questions[questionKey];
        let correctAnswers = 0;
        const correct = answers.map(function (answer) {
          const isCorrect = ['yes', 'true', '1', true, 1].indexOf(Object.values(answer)[0]) != -1;
          if (isCorrect)
            correctAnswers++;
          return isCorrect;
        });
        return {
          question: questionKey,
          answers: answers.map(function (answer) {
            return Object.keys(answer)[0];
          }),
          correct: (correctAnswers != 1) ? correct : correct.reduce((prev, cur, i) => {
            if (prev !== undefined) return prev;
            if (cur === true)
              return i;
            return undefined;
          }, undefined),
          multipleChoice: correctAnswers != 1,
          selection: (correctAnswers != 1) ? answers.map(function () { return false; }) : -1,
        };
      }),
    };
  });

  const newSettings = Object.assign({}, settings, {quizzes: quizzes});

  return `module.exports = ${JSON.stringify(newSettings)};`;
};
