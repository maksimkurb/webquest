/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

const fm = require('front-matter');

module.exports = function frontLoader(source) {
  this.cacheable();

  const frontmatter = fm(source);
  frontmatter.attributes.md = frontmatter.body;

  return `module.exports = ${JSON.stringify(frontmatter.attributes)};`;
};
