---
title: Task
---

**In this WebQuest you will learn:**

- Why healty food is better
- How healthy food can help your health
- And how to make your own diet

We're going to take a look at the short video and discover how healthy food can make us happier. Then we're going to take this knowledge and create a healthy menu. Are you ready? Let's get started!

![Healthy food - Healthy you](/images/healthy-you.png)
