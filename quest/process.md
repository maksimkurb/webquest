---
title: Process
---

### 1. Answer a short questions:

- Would you say you eat a healthy diet? Why/why not?
- How many portions of fruit and vegetables do you eat a day?
- Do you think you eat enough fruit & vegetables?
- How often do you eat takeaways and junk food?
- How often do you eat snacks between meals?
- Do you ever find yourself eating snacks while you are working?
- Do you ever skip breakfast? If yes, how often do you skip breakfast?
- What things could you do to improve your diet? Write down a few ideas.

### 2a. Watch a short video:
<iframe width="560" height="349" src="https://www.youtube-nocookie.com/embed/IZxnhfSQcns?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

### 2b. Answer a questions:
<quiz id="firstQuiz"/>

### 3. Make your own diet!
Diet planning is very important part of your everyday planning. You should be sure, that you eating only healthy products and don't eat too much food, which can be converted in fat.
Here an example of table, which you should make:

<table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
  <thead>
    <tr>
      <td></td><th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th><th>Saturday</th><th>Sunday</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Breakfast</th><td>...</td><td>...</td><td>...</td><td>...</td><td>....</td><td>...</td><td>...</td>
    </tr>
    <tr>
      <th>Snack</th><td>...</td><td>...</td><td>...</td><td>...</td><td>....</td><td>...</td><td>...</td>
    </tr>
    <tr>
      <th>Lunch</th><td>...</td><td>...</td><td>...</td><td>...</td><td>....</td><td>...</td><td>...</td>
    </tr>
    <tr>
      <th>Snack</th><td>...</td><td>...</td><td>...</td><td>...</td><td>....</td><td>...</td><td>...</td>
    </tr>
    <tr>
      <th>Dinner</th><td>...</td><td>...</td><td>...</td><td>...</td><td>....</td><td>...</td><td>...</td>
    </tr>
    <tr>
      <th>Snack</th><td>...</td><td>...</td><td>...</td><td>...</td><td>....</td><td>...</td><td>...</td>
    </tr>
  </tbody>
</table>


**TIP**: you can use a list below to find healthy products. Try to make sure, that your diet includes **various** vitamins!

- **Vitamin A:**
liver, marine animals, fish, butter, egg yolk, cream, fish oil

- **Vitamin D:**
cod liver oil, sardines, herring, salmon, tuna, milk and dairy products

- **Vitamin E:**
oils, liver of animals, eggs, cereals, beans, Brussels sprouts, broccoli, berries rosehips, sea buckthorn, green leafy vegetables, sweet cherry, mountain ash, seeds of apples and pears, sunflower seeds, peanuts, almonds

- **Vitamin K:**
green leafy vegetables, pumpkin, tomatoes, green peas, egg yolk, fish oil, animal liver, soybean oil

- **Vitamin B1:**
dry yeast, bread, peas, cereals, walnuts, peanuts, liver, heart, egg yolk, milk, bran

