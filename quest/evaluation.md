---
title: Evaluation
---

Students will be evaluated on their individual participation in the group, completion of the task required, quality of work, and the presentation of the final menu.

Firstly, let's check quiz answers:

<results/>

Now let's check individual score:

<table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
  <thead>
    <tr>
      <th width="20%"></th>
      <th bgcolor="#ffcc99" width="25%">Shows Potential</th>
      <th bgcolor="#ffffcc" width="25%">Acceptable</th>
      <th bgcolor="#ccffcc" width="25%">Excellent</th>
      <th bgcolor="#ccccff">Score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th width="20%">Group Participation</th>
      <td>Student showed minimal participation in the group.</td>
      <td>Student showed average participation in the group.</td>
      <td>Student was actively involved in the group process.</td>
      <td></td>
    </tr>
    <tr>
      <th width="20%">Completion</th>
      <td>Student only&nbsp;contributed 1 meal or snack to the weekly menu.</td>
      <td>Student only contributed 2-3 meals/snack to the weekly menu.</td>
      <td>Student&nbsp;contributed 3 meals and 1 snack, as required.</td>
      <td></td>
    </tr>
    <tr>
      <th width="20%">Quality</th>
      <td>3 meals/snack contributed were duplicates.</td>
      <td>1-2 meals/snacks contributed&nbsp;were duplicate.</td>
      <td>All of the meals contributed were different.</td>
      <td></td>
    </tr>
    <tr>
      <th width="20%">Presentation</th>
      <td>Menu is starting to show organization, but is still lacking.</td>
      <td>Menu is decently organized</td>
      <td>Menu is clear and well organized.</td>
      <td></td>
    </tr>
  </tbody>
</table>
