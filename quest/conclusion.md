---
title: Conclusion
---

### Congratulations!

Summing up, it must be said that healthy food is crucial to human health. Vegetables, fruits, and other healthy products promote health and make a person happier. Healthy food can be delicious! You must be able to cook it. Properly cooked wholesome food is much tastier than toxic fatty hamburgers.

Now you know how to build your own diet and make your food better!

Great work!
