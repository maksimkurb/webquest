---
title: Introduction
---

[url-healthy]: http://www.cyh.com/HealthTopics/HealthTopicDetailsKids.aspx?p=335&np=152&id=2462
[url-calcium]: http://healthyeating.sfgate.com/wholesome-foods-contain-calcium-1159.html
[url-food-affection]: http://www.besthealthmag.ca/best-you/wellness/how-food-affects-your-mood/
<center>
  <img src="/images/intro-heart.png"/>
</center>

### What is healthy food?
    
Firstly, healthy food is food without chemicals. [Click here][url-healthy] for some facts!

You should eat healthy food, if you want to be healty. If you don't want not to feel tired you should eat healthy food which contains a lot of [calcium][url-calcium]. In this quest you will learn [how food affects us][url-food-affection].

Healty food is very useful. The most important part is diet drawing, which will contain necessary products, required for healthy lifestyle.
