import { QUIZ_SELECT_ANSWER, QUIZ_POPULATE_KEYS } from '../core/constants';
import { quizzes } from '../quest/settings.yml';

export function selectAnswer(quiz, question, selection) {
  return {
    type: QUIZ_SELECT_ANSWER,
    payload: { quiz, question, selection },
  }
}

function populateKeys() {
  return {
    type: QUIZ_POPULATE_KEYS,
    payload: quizzes,
  };
}

function shouldPopulateKeys(state) {
  return state === null;
}

export function loadQuizzes() {
  return (dispatch, getState) => {
    if (!shouldPopulateKeys(getState().quiz))
      return;
    dispatch(populateKeys());
  };
}
