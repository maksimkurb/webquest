
import { QUESTS_PENDING, QUESTS_FULFILLED, QUESTS_REJECTED } from '../core/constants';
import { pages } from '../quest/settings.yml';

function requestQuests() {
  return {
    type: QUESTS_PENDING,
  };
}

function receiveQuests(data) {
  return {
    type: QUESTS_FULFILLED,
    payload: data,
  };
}

function rejectQuests(error) {
  console.error(error);
  return {
    type: QUESTS_REJECTED,
    payload: error,
  };
}

function shouldFetchQuests(state) {
  return !state.loading && !state.steps;
}

export function fetchQuests() {
  return (dispatch, getState) => {
    if (!shouldFetchQuests(getState().quest))
      return;
    dispatch(requestQuests());

      require.ensure([], () => {
        try {
          const quests = pages.map((page) => require(`../quest/${page}.md`));
          const map = {};
          for (let [k, quest] of quests.entries()) {
            map[pages[k]] = quest;
          }
          dispatch(receiveQuests(map));
        } catch (e) {
          alert(`Error occurred during loading quest:\n${e.message}\nSee console for more information`);
          dispatch(rejectQuests(e));
        }
      }, 'quest');

  };
}
